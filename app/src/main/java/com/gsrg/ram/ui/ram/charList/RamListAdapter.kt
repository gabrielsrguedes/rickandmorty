package com.gsrg.ram.ui.ram.charList

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import com.bumptech.glide.RequestManager
import com.gsrg.ram.R
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.ui.customView.LoadingDrawable
import kotlinx.android.synthetic.main.layout_ram_character_list_item.view.*

class RamListAdapter(
    private val requestManager: RequestManager, // Glide
    private val interaction: Interaction?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "RamListAdapter"

    private val VALUE_FOR_PAGINATION = 20

    private var paginationDone: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.layout_ram_character_list_item,
            parent,
            false
        )
        return RamListViewHolder(
            itemView = view,
            requestManager = requestManager,
            interaction = interaction
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder: position $position, paginationDone: $paginationDone")
        if (shouldDoPagination(position)) {
            Log.d(TAG, "onBindViewHolder: set paginationDone TRUE")
            paginationDone = true
            interaction?.doPagination()
        }
        when (holder) {
            is RamListViewHolder -> {
                holder.bind(differ.currentList[position])
            }
        }
    }

    fun submitList(list: List<RamCharData>) {
        val currentListSize = differ.currentList.size
        differ.submitList(list)
        if (paginationDone && list.size > currentListSize) {
            Log.d(TAG, "submitList: set paginationDone FALSE")
            paginationDone = false
        }
    }

    private fun shouldDoPagination(position: Int) =
        !paginationDone && position > differ.currentList.size - VALUE_FOR_PAGINATION

    fun preloadImage(
        requestManager: RequestManager,
        list: List<RamCharData>
    ) {
        for (ramCharData in list) {
            requestManager
                .load(ramCharData.image)
                .preload()
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RamCharData>() {

            override fun areItemsTheSame(
                oldItem: RamCharData,
                newItem: RamCharData
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: RamCharData,
                newItem: RamCharData
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    private val differ = AsyncListDiffer(
        AdapterCallback(this),
        AsyncDifferConfig.Builder(DIFF_CALLBACK).build()
    )

    class RamListViewHolder(
        itemView: View,
        val requestManager: RequestManager,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: RamCharData?) {

            val loadingDrawable = LoadingDrawable(itemView.context)
            loadingDrawable.start()

            item?.let {
                requestManager
                    .load(item.image)
                    .placeholder(loadingDrawable)
                    .into(itemView.imageView)

                itemView.setOnClickListener {
                    interaction?.onItemClicked(item)
                }
                itemView.nameTextView.text = it.name
            }
        }
    }

    internal inner class AdapterCallback(
        private val adapter: RamListAdapter
    ) : ListUpdateCallback {
        override fun onChanged(position: Int, count: Int, payload: Any?) {
            adapter.notifyItemRangeChanged(position, count, payload)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            adapter.notifyDataSetChanged()
        }

        override fun onInserted(position: Int, count: Int) {
            adapter.notifyItemRangeChanged(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            adapter.notifyDataSetChanged()
        }
    }

    interface Interaction {
        fun onItemClicked(item: RamCharData)

        fun doPagination()
    }
}
