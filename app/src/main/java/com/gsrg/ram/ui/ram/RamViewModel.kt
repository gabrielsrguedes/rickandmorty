package com.gsrg.ram.ui.ram

import android.util.Log
import androidx.lifecycle.LiveData
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.models.RamLocationData
import com.gsrg.ram.repository.RamRepository
import com.gsrg.ram.ui.BaseViewModel
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.ui.ram.state.RamStateEvent
import com.gsrg.ram.ui.ram.state.RamViewData
import com.gsrg.ram.util.AbsentLiveData
import javax.inject.Inject

class RamViewModel
@Inject
constructor(
    private val ramRepository: RamRepository
) : BaseViewModel<RamStateEvent, RamViewData>() {

    private val TAG = "RamContextViewModel"

    private var firstTime: Boolean = true

    fun doNewSearch(searchText: String) {
        saveSearchText(searchText)
        savePageNumber(1)
        doSearch()
    }

    fun saveSearchText(searchText: String) {
        val viewDataState = getCurrentViewDataStateOrNew()
        viewDataState.ramCharListViewData.searchText = searchText
        setViewDataState(viewDataState)
    }

    private fun savePageNumber(page: Int) {
        val viewDataState = getCurrentViewDataStateOrNew()
        if (viewDataState.ramCharListViewData.page != page) {
            viewDataState.ramCharListViewData.page = page
            setViewDataState(viewDataState)
        }
    }

    fun doPagination() {
        savePageNumber(getCurrentViewDataStateOrNew().ramCharListViewData.page + 1)
        Log.d(
            TAG,
            "doPagination for page: ${getCurrentViewDataStateOrNew().ramCharListViewData.page}"
        )
        doSearch()
    }

    private fun doSearch() {
        val viewDateState = getCurrentViewDataStateOrNew()
        setStateEvent(
            RamStateEvent.SearchCharListEvent(
                searchText = viewDateState.ramCharListViewData.searchText,
                page = viewDateState.ramCharListViewData.page
            )
        )
    }

    fun updateCacheModeAndCharacterList(viewDataState: RamViewData) {
        val updated = getCurrentViewDataStateOrNew()
        updated.ramCharListViewData.characterListData =
            viewDataState.ramCharListViewData.characterListData
        setViewDataState(updated)
    }

    fun loadFromCacheIfNeeded() {
        if (firstTime) {
            firstTime = false
            setStateEvent(
                RamStateEvent.SearchCharListEvent(
                    searchText = "",
                    page = 1
                )
            )
        }
    }

    fun setSelectedCharacter(ramCharData: RamCharData) {
        val updated = getCurrentViewDataStateOrNew()
        updated.ramCharDetailsViewData.charData = ramCharData
        setViewDataState(updated)
    }

    fun searchForLocationDetails(locationId: Int) {
        setStateEvent(RamStateEvent.SearchLocationEvent(locationId))
    }

    fun updateLocationData(ramLocationData: RamLocationData) {
        val updated = getCurrentViewDataStateOrNew()
        updated.ramCharDetailsViewData.locationData = ramLocationData
        setViewDataState(updated)
    }

    override fun handleStateEvent(stateEvent: RamStateEvent): LiveData<Resource<RamViewData>> {
        return when (stateEvent) {
            is RamStateEvent.SearchAllCharFromDbEvent -> {
                ramRepository.searchForCharactersInDb(
                    currentViewData = getCurrentViewDataStateOrNew()
                )
            }
            is RamStateEvent.SearchCharListEvent -> {
                ramRepository.searchForCharacters(
                    name = stateEvent.searchText,
                    page = stateEvent.page,
                    currentViewData = getCurrentViewDataStateOrNew()
                )
            }
            is RamStateEvent.SearchLocationEvent -> {
                ramRepository.searchForLocation(
                    locationId = stateEvent.locationId,
                    currentViewData = getCurrentViewDataStateOrNew()
                )
            }
            is RamStateEvent.None -> {
                AbsentLiveData.create()
            }
        }
    }

    override fun initNewViewDataState(): RamViewData {
        return RamViewData()
    }
}