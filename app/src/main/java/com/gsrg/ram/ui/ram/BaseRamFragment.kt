package com.gsrg.ram.ui.ram

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gsrg.ram.di.Injectable
import com.gsrg.ram.ui.DataStateChangeListener
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.ui.Status
import com.gsrg.ram.ui.ram.state.RamViewData

open class BaseRamFragment : Fragment(), Injectable {
    private val TAG = "BaseRamFragment"

    lateinit var viewModel: RamViewModel
    lateinit var dependencyProvider: RamDependencyProvider
    lateinit var dataStateChangeListener: DataStateChangeListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProvider(
                this,
                dependencyProvider.getVMProviderFactory()
            ).get(RamViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.resource.observe(
            viewLifecycleOwner,
            Observer { resource: Resource<RamViewData>? ->
                if (resource != null) {
                    dataStateChangeListener.showProgressBar(resource.status == Status.LOADING)
                    if (resource.status == Status.ERROR) {
                        resource.message?.let { message: String ->
                            dataStateChangeListener.showMessage(message)
                        }
                    }
                }
            })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dependencyProvider = context as RamDependencyProvider
        } catch (e: ClassCastException) {
            Log.e(TAG, "$context must implement RamDependencyProvider")
        }
        try {
            dataStateChangeListener = context as DataStateChangeListener
        } catch (e: ClassCastException) {
            Log.e(TAG, "$context must implement DataStateChangeListener")
        }
    }
}