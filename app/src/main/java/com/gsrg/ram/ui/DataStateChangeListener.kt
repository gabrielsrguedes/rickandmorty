package com.gsrg.ram.ui

interface DataStateChangeListener {

    fun showProgressBar(shouldShow: Boolean)

    fun showMessage(message: String)

    fun hideKeyboard()
}