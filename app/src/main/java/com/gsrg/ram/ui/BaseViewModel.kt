package com.gsrg.ram.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<StateEvent, ViewDataState> : ViewModel() {

    private val TAG: String = "BaseViewModel"

    protected val _stateEventLiveData: MutableLiveData<StateEvent> = MutableLiveData()
    protected val _viewDataStateLiveData: MutableLiveData<ViewDataState> = MutableLiveData()

    val viewDataState: LiveData<ViewDataState>
        get() = _viewDataStateLiveData

    val resource: LiveData<Resource<ViewDataState>> = Transformations
        .switchMap(_stateEventLiveData) { stateEvent: StateEvent ->
            stateEvent?.let {
                handleStateEvent(stateEvent)
            }
        }

    fun setStateEvent(event: StateEvent) {
        _stateEventLiveData.value = event
    }

    fun setViewDataState(viewState: ViewDataState) {
        _viewDataStateLiveData.value = viewState
    }

    fun getCurrentViewDataStateOrNew(): ViewDataState {
        return viewDataState.value?.let {
            it
        } ?: initNewViewDataState()
    }

    abstract fun handleStateEvent(stateEvent: StateEvent): LiveData<Resource<ViewDataState>>

    abstract fun initNewViewDataState(): ViewDataState

}