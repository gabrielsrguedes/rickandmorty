package com.gsrg.ram.ui.ram

import com.bumptech.glide.RequestManager
import com.gsrg.ram.viewmodels.ViewModelProviderFactory

interface RamDependencyProvider {

    fun getVMProviderFactory(): ViewModelProviderFactory

    fun getGlideRequestManager(): RequestManager
}