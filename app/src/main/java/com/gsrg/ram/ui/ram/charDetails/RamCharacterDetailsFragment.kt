package com.gsrg.ram.ui.ram.charDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.gsrg.ram.R
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.models.RamLocationData
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.ui.Status
import com.gsrg.ram.ui.customView.LoadingDrawable
import com.gsrg.ram.ui.ram.BaseRamFragment
import com.gsrg.ram.ui.ram.state.RamViewData
import kotlinx.android.synthetic.main.fragment_ram_character_details.*


class RamCharacterDetailsFragment : BaseRamFragment() {

    private val TAG = "RamCharDetailsFragment"

    private lateinit var loadingDrawable: LoadingDrawable
    private var shouldLoadImage = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ram_character_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDrawable = LoadingDrawable(view.context)
        subscribeObservers()
        searchForLocationDetails()
    }

    private fun subscribeObservers() {
        viewModel.resource.observe(
            viewLifecycleOwner,
            Observer { resource: Resource<RamViewData>? ->
                if (resource != null && resource.status != Status.ERROR) {
                    resource.data?.let {
                        it.ramCharDetailsViewData.locationData?.let { ramLocationData: RamLocationData ->
                            viewModel.updateLocationData(ramLocationData)
                        }
                    }
                }
            })

        viewModel.viewDataState.observe(
            viewLifecycleOwner,
            Observer { viewDataState: RamViewData? ->
                if (viewDataState != null) {
                    if (shouldLoadImage) {
                        shouldLoadImage = false
                        viewDataState.ramCharDetailsViewData.charData?.let {
                            dependencyProvider.getGlideRequestManager()
                                .load(it.image)
                                .placeholder(loadingDrawable)
                                .into(imageView)

                            loadingDrawable.start()
                            nameTextView.text = it.name
                            genderTextView.text = it.gender
                            numOfEpisodesTextView.text = it.episodes.toString()
                        }
                    }
                    viewDataState.ramCharDetailsViewData.locationData?.let {
                        locationNameTextView.text = it.name
                        locationTypeTextView.text = it.type
                    }
                }

            })
    }

    private fun searchForLocationDetails() {
        viewModel.viewDataState.value?.let { ramViewData: RamViewData ->
            ramViewData.ramCharDetailsViewData.charData?.let { ramCharData: RamCharData ->
                ramCharData.locationId?.let { locationId: Int ->
                    viewModel.searchForLocationDetails(locationId)
                }
            }
        }
    }

}
