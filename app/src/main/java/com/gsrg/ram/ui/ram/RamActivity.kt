package com.gsrg.ram.ui.ram

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.bumptech.glide.RequestManager
import com.gsrg.ram.R
import com.gsrg.ram.ui.DataStateChangeListener
import com.gsrg.ram.util.MessagesConstants.Companion.NO_INTERNET
import com.gsrg.ram.util.MessagesConstants.Companion.NO_MORE_RESULTS
import com.gsrg.ram.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class RamActivity : DaggerAppCompatActivity(),
    RamDependencyProvider,
    DataStateChangeListener {

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun getToastMessage(message: String) =
        when (message) {
            NO_INTERNET -> getString(R.string.check_internet_connection)
            NO_MORE_RESULTS -> getString(R.string.no_more_results)
            else -> message
        }

    override fun getVMProviderFactory(): ViewModelProviderFactory = providerFactory

    override fun getGlideRequestManager(): RequestManager = requestManager

    override fun showProgressBar(shouldShow: Boolean) {
        progressBar.visibility = if (shouldShow) View.VISIBLE else View.GONE
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, getToastMessage(message), Toast.LENGTH_LONG).show()
    }

    override fun hideKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }
}
