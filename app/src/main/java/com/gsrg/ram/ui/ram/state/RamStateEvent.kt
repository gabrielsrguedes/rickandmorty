package com.gsrg.ram.ui.ram.state

sealed class RamStateEvent {

    class SearchAllCharFromDbEvent : RamStateEvent()

    class SearchCharListEvent(
        val searchText: String,
        val page: Int
    ) : RamStateEvent()

    class SearchLocationEvent(
        val locationId: Int
    ) : RamStateEvent()

    class None : RamStateEvent()
}