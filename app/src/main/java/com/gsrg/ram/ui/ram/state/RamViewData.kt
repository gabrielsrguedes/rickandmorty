package com.gsrg.ram.ui.ram.state

import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.models.RamLocationData

data class RamViewData(
    //RamCharacterListFragment
    var ramCharListViewData: RamCharListViewData = RamCharListViewData(),

    //RamCharacterDetailsFragment
    var ramCharDetailsViewData: RamCharDetailsViewData = RamCharDetailsViewData()
) {
    data class RamCharListViewData(
        var searchText: String = "",
        var page: Int = 1,
        var characterListData: List<RamCharData> = ArrayList()
    ) {
        override fun toString(): String {
            return "RamCharListViewData(searchText='$searchText', page=$page, characterListData=$characterListData)"
        }
    }

    data class RamCharDetailsViewData(
        var charData: RamCharData? = null,
        var locationData: RamLocationData? = null
    ) {
        override fun toString(): String {
            return "RamCharDetailsViewData(charData=$charData, locationData=$locationData)"
        }
    }

    override fun toString(): String {
        return "RamViewData(ramCharListViewData=$ramCharListViewData, ramCharDetailsViewData=$ramCharDetailsViewData)"
    }


}