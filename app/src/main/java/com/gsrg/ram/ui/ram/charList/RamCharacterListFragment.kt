package com.gsrg.ram.ui.ram.charList

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.gsrg.ram.R
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.ui.Status
import com.gsrg.ram.ui.ram.BaseRamFragment
import com.gsrg.ram.ui.ram.state.RamViewData
import kotlinx.android.synthetic.main.fragment_ram_character_list.*


class RamCharacterListFragment : BaseRamFragment(), RamListAdapter.Interaction {

    private val TAG = "RamCharListFragment"

    private lateinit var recyclerAdapter: RamListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ram_character_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeObservers()

        searchButton.setOnClickListener {
            searchText.text.toString().let { text: String ->
                if (text.isNotBlank()) {
                    viewModel.doNewSearch(searchText.text.toString())
                    scrollToBeginning()
                } else {
                    viewModel.saveSearchText(text)
                }
            }
            parentView.requestFocus()
            dataStateChangeListener.hideKeyboard()
        }

        initRecyclerView()
        subscribeObservers()
        viewModel.loadFromCacheIfNeeded()
    }

    private fun subscribeObservers() {
        viewModel.resource.observe(
            viewLifecycleOwner,
            Observer { resource: Resource<RamViewData>? ->
                if (resource != null && resource.status != Status.ERROR) {
                    resource.data?.let {
                        Log.d(TAG, "Resource updated, has data")
                        viewModel.updateCacheModeAndCharacterList(it)
                    }
                }
            })

        viewModel.viewDataState.observe(
            viewLifecycleOwner,
            Observer { viewDataState: RamViewData? ->
                viewDataState?.let {
                    Log.d(TAG, "Update UI: $it")

                    recyclerAdapter.apply {
                        preloadImage(
                            requestManager = dependencyProvider.getGlideRequestManager(),
                            list = it.ramCharListViewData.characterListData
                        )
                        submitList(it.ramCharListViewData.characterListData)
                    }
                    searchText.setText(it.ramCharListViewData.searchText)
                }
            })
    }

    private fun initRecyclerView() {
        recyclerView.apply {
            this.layoutManager = GridLayoutManager(this@RamCharacterListFragment.context, 2)
            recyclerAdapter = RamListAdapter(
                requestManager = dependencyProvider.getGlideRequestManager(),
                interaction = this@RamCharacterListFragment
            )
            Log.d(TAG, "Adding adapter")
            this.adapter = recyclerAdapter
        }
    }

    private fun scrollToBeginning() {
        Log.d(TAG, "scrollToBegining")
        recyclerView.layoutManager?.let {
            it.scrollToPosition(0)
        }
    }

    override fun onItemClicked(item: RamCharData) {
        Log.d(TAG, "onItemClicked: item: $item")
        viewModel.setSelectedCharacter(item)
        findNavController().navigate(R.id.action_ramCharacterListFragment_to_ramCharacterDetailsFragment)
    }

    override fun doPagination() {
        Log.d(TAG, "doPagination")
        viewModel.doPagination()
    }

}
