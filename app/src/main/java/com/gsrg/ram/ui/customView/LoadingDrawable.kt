package com.gsrg.ram.ui.customView

import android.content.Context
import android.os.Build
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.gsrg.ram.R

@Suppress("DEPRECATION")
class LoadingDrawable(context: Context) : CircularProgressDrawable(context) {

    init {
        strokeWidth = 5f
        centerRadius = 30f
        val res = context.resources
        val colorId = R.color.colorAccent
        val color =
            if (Build.VERSION.SDK_INT < 23) res.getColor(colorId) else res.getColor(colorId, null)
        setColorSchemeColors(color)
    }
}