package com.gsrg.ram.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.gsrg.ram.BaseApplication
import dagger.android.AndroidInjection
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection

object AppInjector {

    fun init(app: BaseApplication) {
        DaggerAppComponent.builder().application(app).build().inject(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
                // Not used in this case
            }

            override fun onActivityResumed(activity: Activity?) {
                // Not used in this case
            }

            override fun onActivityStarted(activity: Activity?) {
                // Not used in this case
            }

            override fun onActivityDestroyed(activity: Activity?) {
                // Not used in this case
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
                // Not used in this case
            }

            override fun onActivityStopped(activity: Activity?) {
                // Not used in this case
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                activity?.let {
                    handleActivity(it)
                }
            }

        })
    }

    private fun handleActivity(activity: Activity) {

        if (activity is HasAndroidInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(object :
                FragmentManager.FragmentLifecycleCallbacks() {
                override fun onFragmentCreated(
                    fm: FragmentManager,
                    f: Fragment,
                    savedInstanceState: Bundle?
                ) {
                    if (f is Injectable) {
                        AndroidSupportInjection.inject(f)
                    }
                }
            }, true)
        }
    }
}