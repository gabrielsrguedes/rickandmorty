package com.gsrg.ram.di.rickAndMorty

import com.gsrg.ram.ui.ram.charDetails.RamCharacterDetailsFragment
import com.gsrg.ram.ui.ram.charList.RamCharacterListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RamFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRamCharacterListFragment(): RamCharacterListFragment

    @ContributesAndroidInjector
    abstract fun contributeRamCharacterDetailsFragment(): RamCharacterDetailsFragment
}