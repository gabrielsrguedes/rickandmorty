package com.gsrg.ram.di.rickAndMorty

import androidx.lifecycle.ViewModel
import com.gsrg.ram.di.ViewModelKey
import com.gsrg.ram.ui.ram.RamViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RamViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RamViewModel::class)
    abstract fun bindRamViewModel(ramViewModel: RamViewModel): ViewModel
}