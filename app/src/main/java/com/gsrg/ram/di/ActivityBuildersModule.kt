package com.gsrg.ram.di

import com.gsrg.ram.di.rickAndMorty.RamFragmentBuildersModule
import com.gsrg.ram.di.rickAndMorty.RamModule
import com.gsrg.ram.di.rickAndMorty.RamScope
import com.gsrg.ram.di.rickAndMorty.RamViewModelModule
import com.gsrg.ram.ui.ram.RamActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @RamScope
    @ContributesAndroidInjector(
        modules = [RamModule::class, RamFragmentBuildersModule::class, RamViewModelModule::class]
    )
    abstract fun contributeRamActivity(): RamActivity
}