package com.gsrg.ram.di.rickAndMorty

import com.gsrg.ram.api.RamApiService
import com.gsrg.ram.persistence.AppDatabase
import com.gsrg.ram.persistence.RamCharDataDao
import com.gsrg.ram.persistence.RamLocationDataDao
import com.gsrg.ram.repository.RamRepository
import com.gsrg.ram.session.SessionManager
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RamModule {

    @RamScope
    @Provides
    fun provideRamCharDataDao(database: AppDatabase): RamCharDataDao {
        return database.getRamCharDataDao()
    }

    @RamScope
    @Provides
    fun provideRamLocationDataDao(database: AppDatabase): RamLocationDataDao {
        return database.getRamLocationDataDao()
    }

    @RamScope
    @Provides
    fun provideRamApiService(retrofitBuilder: Retrofit.Builder): RamApiService {
        return retrofitBuilder
            .build()
            .create(RamApiService::class.java)
    }

    @RamScope
    @Provides
    fun provideRamRepository(
        ramApiService: RamApiService,
        ramCharDataDao: RamCharDataDao,
        ramLocationDataDao: RamLocationDataDao,
        sessionManager: SessionManager
    ): RamRepository {
        return RamRepository(
            ramApiService = ramApiService,
            ramCharDataDao = ramCharDataDao,
            ramLocationDataDao = ramLocationDataDao,
            sessionManager = sessionManager
        )
    }
}