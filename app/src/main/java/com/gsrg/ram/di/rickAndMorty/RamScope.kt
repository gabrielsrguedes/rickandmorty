package com.gsrg.ram.di.rickAndMorty

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class RamScope