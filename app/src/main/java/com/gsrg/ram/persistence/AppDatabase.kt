package com.gsrg.ram.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.models.RamLocationData


@Database(
    entities = [RamCharData::class, RamLocationData::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getRamCharDataDao(): RamCharDataDao
    abstract fun getRamLocationDataDao(): RamLocationDataDao

    companion object {

        const val DATABASE_NAME = "app_db"
    }
}
