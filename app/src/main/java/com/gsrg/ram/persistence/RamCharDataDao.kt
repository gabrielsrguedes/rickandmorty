package com.gsrg.ram.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_ID
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_NAME
import com.gsrg.ram.util.RoomConstants.Companion.RAM_CHAR_DATA_TABLE

@Dao
interface RamCharDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ramCharData: RamCharData): Long

    @Query("SELECT * FROM $RAM_CHAR_DATA_TABLE")
    fun selectAll(): LiveData<List<RamCharData>>

    @Query("SELECT * FROM $RAM_CHAR_DATA_TABLE WHERE $CHAR_NAME LIKE '%' || :name || '%'")
    fun selectAllWithName(name: String): LiveData<List<RamCharData>>

    @Query("SELECT * FROM $RAM_CHAR_DATA_TABLE WHERE $CHAR_ID = :charId")
    fun selectChar(charId: Int): LiveData<List<RamCharData>>

    @Query("DELETE FROM $RAM_CHAR_DATA_TABLE")
    fun deleteAll()
}