package com.gsrg.ram.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gsrg.ram.models.RamLocationData
import com.gsrg.ram.util.RoomConstants.Companion.LOC_ID
import com.gsrg.ram.util.RoomConstants.Companion.RAM_LOCATION_DATA_TABLE

@Dao
interface RamLocationDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ramLocationData: RamLocationData): Long

    @Query("SELECT * FROM $RAM_LOCATION_DATA_TABLE WHERE $LOC_ID = :locId")
    fun selectLocation(locId: Int): LiveData<List<RamLocationData>>

    @Query("DELETE FROM $RAM_LOCATION_DATA_TABLE")
    fun deleteAll()
}