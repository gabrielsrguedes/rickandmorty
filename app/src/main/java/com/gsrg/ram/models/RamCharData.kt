package com.gsrg.ram.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_EPISODES
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_GENDER
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_ID
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_IMAGE
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_LOCATION_ID
import com.gsrg.ram.util.RoomConstants.Companion.CHAR_NAME
import com.gsrg.ram.util.RoomConstants.Companion.RAM_CHAR_DATA_TABLE
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = RAM_CHAR_DATA_TABLE)
data class RamCharData(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = CHAR_ID)
    var id: Int,

    @ColumnInfo(name = CHAR_NAME)
    var name: String? = "",

    @ColumnInfo(name = CHAR_IMAGE)
    var image: String? = "",

    @ColumnInfo(name = CHAR_GENDER)
    val gender: String? = "",

    @ColumnInfo(name = CHAR_EPISODES)
    val episodes: Int? = 0,

    @ColumnInfo(name = CHAR_LOCATION_ID)
    val locationId: Int? = 0
) : Parcelable {
    override fun toString(): String {
        return "RamCharData(id=$id, name=$name, image=$image, gender=$gender, episodes=$episodes, locationId=$locationId)"
    }
}