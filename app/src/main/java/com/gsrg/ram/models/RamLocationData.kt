package com.gsrg.ram.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gsrg.ram.util.RoomConstants.Companion.LOC_ID
import com.gsrg.ram.util.RoomConstants.Companion.LOC_NAME
import com.gsrg.ram.util.RoomConstants.Companion.LOC_TYPE
import com.gsrg.ram.util.RoomConstants.Companion.RAM_LOCATION_DATA_TABLE
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = RAM_LOCATION_DATA_TABLE)
data class RamLocationData(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = LOC_ID)
    var id: Int,

    @ColumnInfo(name = LOC_NAME)
    var name: String? = "",

    @ColumnInfo(name = LOC_TYPE)
    var type: String? = ""
) : Parcelable {
    override fun toString(): String {
        return "LocationData(id=$id, name=$name, type=$type)"
    }
}