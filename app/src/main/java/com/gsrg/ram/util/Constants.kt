package com.gsrg.ram.util

class ApiConstants {

    companion object {
        const val BASE_URL = "https://rickandmortyapi.com/api/"
        const val CHARACTER = "character/"
        const val NAME = "name"
        const val LOCATION = "location/"
        const val PAGE = "page"
    }
}

class RoomConstants {

    companion object {
        const val RAM_CHAR_DATA_TABLE = "ram_char_data"
        const val CHAR_ID = "id"
        const val CHAR_NAME = "name"
        const val CHAR_IMAGE = "image"
        const val CHAR_GENDER = "gender"
        const val CHAR_EPISODES = "episodes"
        const val CHAR_LOCATION_ID = "locationId"

        const val RAM_LOCATION_DATA_TABLE = "ram_location_data"
        const val LOC_ID = "id"
        const val LOC_NAME = "name"
        const val LOC_TYPE = "type"
    }
}

class MessagesConstants {

    companion object {
        const val NO_INTERNET = "no internet"
        const val NO_MORE_RESULTS = "{\"error\":\"There is nothing here\"}"
    }
}