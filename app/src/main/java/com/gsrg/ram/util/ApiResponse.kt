package com.gsrg.ram.util

import android.util.Log
import retrofit2.Response

/**
 * Copied from Architecture components google sample:
 * https://github.com/googlesamples/android-architecture-components/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/api/ApiResponse.kt
 */
@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {

    companion object {
        private const val TAG: String = "RamApiResp"
        private const val unknownError = "unknown error"


        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            Log.e(TAG, error.message ?: unknownError)
            return ApiErrorResponse(
                error.message ?: unknownError
            )
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {

            Log.d(TAG, "response: $response")
            Log.d(TAG, "raw: ${response.raw()}")
            Log.d(TAG, "headers: ${response.headers()}")
            Log.d(TAG, "message: ${response.message()}")

            if (response.isSuccessful) {
                val body = response.body()
                return if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(body = body)
                }
            } else {
                val msg = response.errorBody()?.string()
                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }
                return ApiErrorResponse(
                    errorMsg ?: unknownError
                )
            }
        }
    }
}

/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorMessage: String) : ApiResponse<T>()
