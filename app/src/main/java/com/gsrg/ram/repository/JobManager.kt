package com.gsrg.ram.repository

import android.util.Log
import kotlinx.coroutines.Job

open class JobManager(private val jobName: String) {

    private val TAG = "RamJobManager"

    val jobs: HashMap<String, Job> = HashMap()

    fun addJob(jobName: String, job: Job) {
        Log.d(TAG, "add job: $jobName")
        if (jobs.containsKey(jobName)) {
            cancelJob(jobName)
        }
        jobs[jobName] = job
    }

    private fun cancelJob(jobName: String) {
        Log.d(TAG, "cancel job: $jobName")
        getJob(jobName)?.cancel()
    }

    private fun getJob(jobName: String): Job? {
        if (jobs.containsKey(jobName)) {
            jobs[jobName]?.let {
                return it
            }
        }
        return null
    }

    fun cancelActiveJobs() {
        for ((jobName, job) in jobs) {
            if (job.isActive) {
                Log.d(TAG, "${this.jobName}: cancelling job: $jobName")
                job.cancel()
            }
        }
    }
}