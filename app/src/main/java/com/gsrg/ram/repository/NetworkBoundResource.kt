package com.gsrg.ram.repository

import android.util.Log
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.util.ApiEmptyResponse
import com.gsrg.ram.util.ApiErrorResponse
import com.gsrg.ram.util.ApiResponse
import com.gsrg.ram.util.ApiSuccessResponse
import com.gsrg.ram.util.MessagesConstants.Companion.NO_INTERNET
import kotlinx.coroutines.*

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */

abstract class NetworkBoundResource<ResultType, CacheType, RequestType>
@MainThread constructor(
    isNetworkAvailable: Boolean, // is there a network connection?
    isNetworkRequest: Boolean,   // is this a network request? Does it need network connection?
    shouldLoadFromDb: Boolean    // should the cached data be loaded
) {

    private val TAG = "RamNBR"

    protected val result = MediatorLiveData<Resource<RequestType>>()
    protected lateinit var job: CompletableJob
    protected lateinit var coroutineScope: CoroutineScope

    init {
        setJob(initNewJob())
        setValue(Resource.loading(null))

        if (shouldLoadFromDb) {
            val dbSource = loadFromDb()
            result.addSource(dbSource) { data: RequestType ->
                result.removeSource(dbSource)
                setValue(Resource.loading(data))
                if (isNetworkRequest && !isNetworkAvailable) {
                    onCompleteJob(
                        Resource.error(
                            msg = NO_INTERNET,
                            data = null
                        )
                    )
                }
            }
        }

        if (isNetworkRequest) {
            if (isNetworkAvailable) {
                fetchFromNetwork()
            } else {
                onCompleteJob(
                    Resource.error(
                        msg = NO_INTERNET,
                        data = null
                    )
                )
            }
        } else {
            dbRequest()
        }
    }

    private fun dbRequest() {
        coroutineScope.launch {
            createDbRequestAndReturn()
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response: ApiResponse<ResultType> ->
            result.removeSource(apiResponse)
            coroutineScope.launch {
                handleNetworkCall(response)
            }
        }
    }

    private suspend fun handleNetworkCall(response: ApiResponse<ResultType>?) {
        when (response) {
            is ApiSuccessResponse -> {
                Log.d(TAG, "Request success")
                handleApiSuccessResponse(response)
            }
            is ApiErrorResponse -> {
                Log.e(TAG, response.errorMessage)
                onCompleteJob(
                    Resource.error(
                        msg = response.errorMessage,
                        data = null
                    )
                )
            }
            is ApiEmptyResponse -> {
                Log.e(TAG, "Request returned NOTHING")
                onCompleteJob(
                    Resource.error(
                        msg = "Request returned NOTHING",
                        data = null
                    )
                )
            }
        }
    }

    fun onCompleteJob(resultValue: Resource<RequestType>) {
        GlobalScope.launch(Dispatchers.Main) {
            job.complete()
            setValue(resultValue)
        }
    }

    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    @UseExperimental(InternalCoroutinesApi::class)
    private fun initNewJob(): Job {
        Log.d(TAG, "initNewJob: called...")
        job = Job()
        job.invokeOnCompletion(
            onCancelling = true,
            invokeImmediately = true,
            handler = object : CompletionHandler {

                override fun invoke(cause: Throwable?) {
                    if (job.isCancelled) {
                        Log.e(TAG, "Job has been cancelled.")
                    } else if (job.isCompleted) {
                        Log.d(TAG, "Job has been completed")
                        // Do nothing. Should be handled already
                    }
                }
            })
        coroutineScope =
            CoroutineScope(Dispatchers.IO + job) // creates an specific scope for the job and is IO. This allows to do job.cancel(), for example
        return job
    }

    fun asLiveData() = result as LiveData<Resource<RequestType>>

    abstract suspend fun createDbRequestAndReturn()

    abstract suspend fun handleApiSuccessResponse(response: ApiSuccessResponse<ResultType>)

    abstract fun createCall(): LiveData<ApiResponse<ResultType>>

    abstract fun loadFromDb(): LiveData<RequestType>

    abstract suspend fun updateLocalDb(cacheType: CacheType?)

    abstract fun setJob(job: Job)
}
