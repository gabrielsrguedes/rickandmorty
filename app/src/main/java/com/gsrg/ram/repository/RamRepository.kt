package com.gsrg.ram.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.switchMap
import com.gsrg.ram.api.RamApiService
import com.gsrg.ram.api.responses.RamBaseListResponse
import com.gsrg.ram.api.responses.RamLocation
import com.gsrg.ram.models.RamCharData
import com.gsrg.ram.models.RamLocationData
import com.gsrg.ram.persistence.RamCharDataDao
import com.gsrg.ram.persistence.RamLocationDataDao
import com.gsrg.ram.session.SessionManager
import com.gsrg.ram.ui.Resource
import com.gsrg.ram.ui.ram.state.RamViewData
import com.gsrg.ram.util.AbsentLiveData
import com.gsrg.ram.util.ApiResponse
import com.gsrg.ram.util.ApiSuccessResponse
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RamRepository
@Inject
constructor(
    val ramApiService: RamApiService,
    val ramCharDataDao: RamCharDataDao,
    val ramLocationDataDao: RamLocationDataDao,
    val sessionManager: SessionManager
) : JobManager("RamRepository"), IRamRepository {

    private val TAG = "RamRepository"

    fun searchForCharacters(
        name: String,
        page: Int,
        currentViewData: RamViewData
    ): LiveData<Resource<RamViewData>> {
        return object :
            NetworkBoundResource<RamBaseListResponse, List<RamCharData>, RamViewData>(
                isNetworkAvailable = sessionManager.isConnectedToTheInternet(),
                isNetworkRequest = true,
                shouldLoadFromDb = true
            ) {
            override suspend fun createDbRequestAndReturn() {
                withContext(Main) {
                    result.addSource(loadFromDb()) { ramCharListViewData: RamViewData ->
                        onCompleteJob(Resource.success(data = ramCharListViewData))
                    }
                }
            }

            override suspend fun handleApiSuccessResponse(response: ApiSuccessResponse<RamBaseListResponse>) {
                response.body.charList?.let {
                    val updatedRamCharDataList = ArrayList<RamCharData>()

                    for (ramCharacter in it) {
                        updatedRamCharDataList.add(
                            RamCharData(
                                id = ramCharacter.id,
                                name = ramCharacter.name,
                                gender = ramCharacter.gender,
                                image = ramCharacter.image,
                                episodes = ramCharacter.numOfEpisodes(),
                                locationId = ramCharacter.location?.let { ramLocation: RamLocation ->
                                    ramLocation.locationId()
                                } ?: 0
                            )
                        )
                    }

                    updateLocalDb(updatedRamCharDataList)
                    createDbRequestAndReturn()
                }
            }

            override fun createCall(): LiveData<ApiResponse<RamBaseListResponse>> {
                return ramApiService.getRamCharList(name, page)
            }

            override fun loadFromDb(): LiveData<RamViewData> {
                return if (name.isEmpty()) {
                    ramCharDataDao.selectAll()
                        .switchMap { charList: List<RamCharData> ->
                            object : LiveData<RamViewData>() {
                                override fun onActive() {
                                    super.onActive()
                                    currentViewData.ramCharListViewData.characterListData = charList
                                    value = currentViewData
                                }
                            }
                        }
                } else {
                    ramCharDataDao.selectAllWithName(name)
                        .switchMap { charList: List<RamCharData> ->
                            object : LiveData<RamViewData>() {
                                override fun onActive() {
                                    super.onActive()
                                    currentViewData.ramCharListViewData.characterListData = charList
                                    value = currentViewData
                                }
                            }
                        }
                }
            }

            override suspend fun updateLocalDb(cacheType: List<RamCharData>?) {
                cacheType?.let {
                    for (ramCharData in cacheType) {
                        ramCharDataDao.insert(ramCharData)
                    }
                }
            }

            override fun setJob(job: Job) {
                addJob("searchForCharacters", job)
            }
        }.asLiveData()
    }

    fun searchForCharactersInDb(
        currentViewData: RamViewData
    ): LiveData<Resource<RamViewData>> {
        return object :
            NetworkBoundResource<RamBaseListResponse, List<RamCharData>, RamViewData>(
                isNetworkAvailable = sessionManager.isConnectedToTheInternet(),
                isNetworkRequest = false,
                shouldLoadFromDb = true
            ) {
            override suspend fun createDbRequestAndReturn() {
                withContext(Main) {
                    result.addSource(loadFromDb()) { ramCharListViewData: RamViewData ->
                        onCompleteJob(Resource.success(data = ramCharListViewData))
                    }
                }
            }

            override suspend fun handleApiSuccessResponse(response: ApiSuccessResponse<RamBaseListResponse>) {
                // Not used in this case
            }

            override fun createCall(): LiveData<ApiResponse<RamBaseListResponse>> {
                // Not used in this case
                return AbsentLiveData.create()
            }

            override fun loadFromDb(): LiveData<RamViewData> {
                return ramCharDataDao.selectAll()
                    .switchMap { charList: List<RamCharData> ->
                        object : LiveData<RamViewData>() {
                            override fun onActive() {
                                super.onActive()
                                currentViewData.ramCharListViewData.characterListData = charList
                                value = currentViewData
                            }
                        }

                    }
            }

            override suspend fun updateLocalDb(cacheType: List<RamCharData>?) {
                // Not used in this case
            }

            override fun setJob(job: Job) {
                addJob("searchForCharactersInDb", job)
            }
        }.asLiveData()
    }

    fun searchForLocation(
        locationId: Int,
        currentViewData: RamViewData
    ): LiveData<Resource<RamViewData>> {
        return object :
            NetworkBoundResource<RamLocation, RamLocationData, RamViewData>(
                isNetworkAvailable = sessionManager.isConnectedToTheInternet(),
                isNetworkRequest = true,
                shouldLoadFromDb = true
            ) {
            override suspend fun createDbRequestAndReturn() {
                withContext(Main) {
                    result.addSource(loadFromDb()) { ramViewData: RamViewData ->
                        onCompleteJob(Resource.success(data = ramViewData))
                    }
                }
            }

            override suspend fun handleApiSuccessResponse(response: ApiSuccessResponse<RamLocation>) {
                response.body.let {
                    val ramLocationData = RamLocationData(
                        id = it.locationId(),
                        name = it.name,
                        type = it.type
                    )
                    updateLocalDb(ramLocationData)
                    createDbRequestAndReturn()
                }
            }

            override fun createCall(): LiveData<ApiResponse<RamLocation>> {
                return ramApiService.getRamLocation(locationId)
            }

            override fun loadFromDb(): LiveData<RamViewData> {
                return ramLocationDataDao.selectLocation(locationId)
                    .switchMap { locList: List<RamLocationData> ->
                        object : LiveData<RamViewData>() {
                            override fun onActive() {
                                super.onActive()
                                if (locList.isNotEmpty()) {
                                    currentViewData.ramCharDetailsViewData.locationData = locList[0]
                                    value = currentViewData
                                }
                            }
                        }
                    }
            }

            override suspend fun updateLocalDb(cacheType: RamLocationData?) {
                cacheType?.let {
                    ramLocationDataDao.insert(it)
                }
            }

            override fun setJob(job: Job) {
                addJob("searchForLocation", job)
            }

        }.asLiveData()
    }
}