package com.gsrg.ram.api

import androidx.lifecycle.LiveData
import com.gsrg.ram.api.responses.RamBaseListResponse
import com.gsrg.ram.api.responses.RamCharacter
import com.gsrg.ram.api.responses.RamLocation
import com.gsrg.ram.util.ApiConstants.Companion.CHARACTER
import com.gsrg.ram.util.ApiConstants.Companion.LOCATION
import com.gsrg.ram.util.ApiConstants.Companion.NAME
import com.gsrg.ram.util.ApiConstants.Companion.PAGE
import com.gsrg.ram.util.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RamApiService {

    @GET(CHARACTER)
    fun getRamCharList(
        @Query(NAME) name: String,
        @Query(PAGE) page: Int
    ): LiveData<ApiResponse<RamBaseListResponse>>

    @GET("$CHARACTER{id}")
    fun getRamCharacter(
        @Path("id") id: Int
    ): LiveData<ApiResponse<RamCharacter>>

    @GET("$LOCATION{id}")
    fun getRamLocation(
        @Path("id") id: Int
    ): LiveData<ApiResponse<RamLocation>>
}