package com.gsrg.ram.api.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RamLocation(

    @SerializedName("id")
    @Expose
    val id: Int? = null,

    @SerializedName("name")
    @Expose
    val name: String? = null,

    @SerializedName("type")
    @Expose
    val type: String? = null,

    @SerializedName("url")
    @Expose
    val url: String? = null,

    @SerializedName("error")
    @Expose
    val error: String? = null
) {
    fun locationId(): Int {
        return id?.let {
            it
        } ?: url?.let {
            if (it.isNotBlank()) {
                it.substringAfterLast("/").toIntOrNull()?.let { id: Int ->
                    id
                }
            } else {
                0
            }
        } ?: 0
    }
}
