package com.gsrg.ram.api.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RamCharacter(

    @SerializedName("id")
    @Expose
    val id: Int,

    @SerializedName("name")
    @Expose
    val name: String? = null,

    @SerializedName("gender")
    @Expose
    val gender: String? = null,

    @SerializedName("location")
    @Expose
    val location: RamLocation? = null,

    @SerializedName("image")
    @Expose
    val image: String? = null,

    @SerializedName("episode")
    @Expose
    val episode: List<String>? = null,

    @SerializedName("error")
    @Expose
    val error: String? = null
) {
    fun numOfEpisodes(): Int {
        return episode?.size ?: 0
    }
}
