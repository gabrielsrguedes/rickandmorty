package com.gsrg.ram.api.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RamBaseListResponse(

    @SerializedName("results")
    @Expose
    val charList: List<RamCharacter>? = null,

    @SerializedName("error")
    @Expose
    val error: String? = null
)