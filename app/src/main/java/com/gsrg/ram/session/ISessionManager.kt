package com.gsrg.ram.session

interface ISessionManager {

    fun isConnectedToTheInternet(): Boolean
}